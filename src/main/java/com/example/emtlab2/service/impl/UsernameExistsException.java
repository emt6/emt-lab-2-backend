package com.example.emtlab2.service.impl;

public class UsernameExistsException extends RuntimeException {
    public UsernameExistsException(String username) {
        super(String.format("Username: %s already exists.", username));
    }
}
