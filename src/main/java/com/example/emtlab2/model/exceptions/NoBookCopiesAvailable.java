package com.example.emtlab2.model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NoBookCopiesAvailable extends RuntimeException {
    public NoBookCopiesAvailable(Long id) {
        super(String.format("No copies of the book with id %d are available.", id));
    }
}
